####################################################
# START GLOBAL DECLARATION
####################################################
ARG HOSTNAME_NAME_DEFAULT=localhost
ARG SERVICE_NAME_DEFAULT=metadataHub
ARG SERVICE_PORT_DEFAULT=8890
ARG SERVICE_ROOT_DIRECTORY_DEFAULT=/spring/
####################################################
# END GLOBAL DECLARATION
####################################################

####################################################
# Building environment (java & git)
####################################################
FROM openjdk:16-bullseye AS build-env-java
LABEL maintainer=webmaster@datamanager.kit.edu
LABEL stage=build-env

# Install git as additional requirement
RUN apt-get -y update && \
    apt-get -y upgrade  && \
    apt-get install -y --no-install-recommends git bash && \
    apt-get clean \
    && rm -rf /var/lib/apt/lists/*

####################################################
# Building service
####################################################
FROM build-env-java AS build-service-metadatahub
LABEL maintainer=webmaster@datamanager.kit.edu
LABEL stage=build-contains-sources

# Fetch arguments from above
ARG SERVICE_NAME_DEFAULT
ARG SERVICE_ROOT_DIRECTORY_DEFAULT
ARG HOSTNAME_DEFAULT

# Declare environment variables
ENV SERVICE_NAME=${SERVICE_NAME_DEFAULT}
ENV SERVICE_DIRECTORY=$SERVICE_ROOT_DIRECTORY_DEFAULT$SERVICE_NAME
ENV HOSTNAME=$HOSTNAME_DEFAULT

# Create directory for repo
RUN mkdir -p /git/${SERVICE_NAME}
WORKDIR /git/${SERVICE_NAME}
COPY . .
# Build service in given directory
RUN bash ./build.sh $SERVICE_DIRECTORY $HOSTNAME

####################################################
# Runtime environment 4 turntable
####################################################
FROM openjdk:16-bullseye AS run-service-metadatahub
LABEL maintainer=webmaster@datamanager.kit.edu
LABEL stage=run

# Fetch arguments from above
ARG SERVICE_NAME_DEFAULT
ARG SERVICE_PORT_DEFAULT
ARG SERVICE_ROOT_DIRECTORY_DEFAULT

# Declare environment variables
ENV SERVICE_NAME=${SERVICE_NAME_DEFAULT}
ENV SERVICE_DIRECTORY=${SERVICE_ROOT_DIRECTORY_DEFAULT}${SERVICE_NAME}
ENV SERVICE_PORT=${SERVICE_PORT_DEFAULT}

# Install bash as additional requirement
RUN apt-get -y update && \
    apt-get -y upgrade  && \
    apt-get install -y --no-install-recommends bash && \
    apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Copy service from build container
RUN mkdir -p ${SERVICE_DIRECTORY}
WORKDIR ${SERVICE_DIRECTORY}
COPY --from=build-service-metadatahub ${SERVICE_DIRECTORY} ./

# Define rest port 
EXPOSE ${SERVICE_PORT}
# Define doip port 
EXPOSE 8880
ENTRYPOINT ["bash", "./run.sh"]
