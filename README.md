# MetadataHub

Acts as a facade for multiple instances of a metadata repository.
The server implements the Digital Object Interface Protocol specifier by DONA (DOIP).
For adding a new repository some code might be necessary.
1. New repositories have to be added via configuration.
2. Add code for transforming datacite metadata used by DOIP into proprietary metadata used by repository and vice versa.
3. Add code for serving repository
    - In case of an existing REST interface some Configuration might be sufficient.

# Documentation
ToDo
## Start service
To start service just type './gradlew bootRun'.

## API provided by DOIP
See [Turntable API](https://nfdi4ing.pages.rwth-aachen.de/s-3/s-3-3/turntable-interface/) for further details.

## How to add a new repository
ToDo

## How to use SimpleServiceClient
See code snippets [here](doc/ExampleUsingSimpleServiceClient.md)

## How to access DoipServer
See code snippets [here](doc/ExampleDoipClient.md)

## How to access RESTServer
There is also an embedded  REST server which delivers all available mappingIDs
(State 05/2022).
It is available via port 8080.

### SwaggerUI
The REST API is now available in browser:
http://localhost:8099/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config

## Dockerize Service
### Setup Docker Image
To dockerize service you just have to execute the shell script in the docker
directory and provide at least the server name of the resulting container.
```
user@localhost:/home/user/metadatahub/docker$ bash buildDocker.sh hostname_of_the_server
```
### Setup Network
```
user@localhost:/home/user/metadatahub/docker$ docker network create network4metadatahub
```
### Create Docker Container
```
user@localhost:/home/user/metadatahub/docker$ docker run --network network4metadatahub -d -p8888:8888 -p8890:8890 kitdm/metadatahub:...
```
In case of a local container determine the ip address of the container. (e.g.: 172.17.0.1).
The service should be reachable via this ip.

# Acknowledgements
The authors would like to thank the Federal Government and the Heads of Government 
of the Länder, as well as the Joint Science Conference (GWK), for their funding and 
support within the framework of the NFDI4Ing consortium. 
Funded by the German Research Foundation (DFG) - project number 442146713. 

# Links
- [DOIP Specification] (https://www.dona.net/sites/default/files/2018-11/DOIPv2Spec_1.pdf)
- [DOIP SDK] (https://www.dona.net/sites/default/files/2020-09/DOIPv2SDKOverview.pdf)
- [Turntable API](https://nfdi4ing.pages.rwth-aachen.de/s-3/s-3-3/turntable-interface/)
