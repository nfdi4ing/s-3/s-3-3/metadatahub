/*
 * Copyright 2021 Karlsruhe Institute of Technology.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.kit.metadatahub.doip.mapping.metadata.impl;

import edu.kit.metadatahub.doip.mapping.metadata.IMetadataMapper;
import edu.kit.turntable.mapping.Datacite43Schema;
import edu.kit.turntable.mapping.Date;
import edu.kit.turntable.mapping.Identifier;
import edu.kit.turntable.mapping.RelatedIdentifier;
import edu.kit.turntable.mapping.RelatedResource;
import edu.kit.turntable.mapping.Schema;
import edu.kit.turntable.mapping.SchemaRecordSchema;
import edu.kit.turntable.mapping.Title;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Map SchemaRecord to datacite.
 */
public class SchemaRecordMapper implements IMetadataMapper<SchemaRecordSchema> {

  /**
   * Logger.
   */
  private final static Logger LOGGER = LoggerFactory.getLogger(SchemaRecordMapper.class);

  /**
   * Parser for ISO date format.
   */
  private final static SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

  @Override
  public SchemaRecordSchema mapFromDatacite(Datacite43Schema datacite) {
    SchemaRecordSchema metadata = new SchemaRecordSchema();
    metadata.setSchemaId(datacite.getTitles().iterator().next().getTitle());
    metadata.setType(SchemaRecordSchema.Type.fromValue(datacite.getFormats().iterator().next()));
    try {
      for (Date item : datacite.getDates()) {
        if (item.getDateType().equals(Date.DateType.CREATED)) {
          metadata.setCreatedAt(dateformat.parse(item.getDate()));
        }
        if (item.getDateType().equals(Date.DateType.UPDATED)) {
          metadata.setLastUpdate(dateformat.parse(item.getDate()));
        }
      }
    } catch (ParseException ex) {
      java.util.logging.Logger.getLogger(MetadataRecordMapper.class.getName()).log(Level.SEVERE, null, ex);
    }
    for (Identifier identifier : datacite.getIdentifiers()) {
      if (identifier.getIdentifierType().equals("URL")) {
        metadata.setSchemaDocumentUri(identifier.getIdentifier());
      }
    }

    return metadata;
  }

  @Override
  public Datacite43Schema mapToDatacite(Object metadataObject) {
    SchemaRecordSchema metadata = (SchemaRecordSchema) metadataObject;
    Datacite43Schema datacite = new Datacite43Schema();
    datacite.getFormats().add(metadata.getType().value());
    Title title = new Title();
    title.setTitle(metadata.getSchemaId());
    title.setTitleType(Title.TitleType.OTHER);
    datacite.getTitles().add(title);
    Date dates = new Date();
    dates.setDate(metadata.getCreatedAt().toString());
    dates.setDateType(Date.DateType.CREATED);
    Date upDate = new Date();
    upDate.setDate(metadata.getLastUpdate().toString());
    upDate.setDateType(Date.DateType.UPDATED);
    datacite.getDates().add(dates);
    datacite.getDates().add(upDate);
    Identifier identifier = new Identifier();
    identifier.setIdentifier(metadata.getSchemaId());//Pid().getIdentifier());
    if ((metadata.getPid() != null) && (metadata.getPid().getIdentifierType() != null)) {
      identifier.setIdentifierType(metadata.getPid().getIdentifierType());
    } else {
      identifier.setIdentifierType("Handle");
    }
    datacite.getIdentifiers().add(identifier);

    identifier = new Identifier();
    identifier.setIdentifier(metadata.getSchemaDocumentUri());//Pid().getIdentifier());
    identifier.setIdentifierType("URL");

    datacite.getIdentifiers().add(identifier);

    return datacite;
  }

}
