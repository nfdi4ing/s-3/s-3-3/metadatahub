/*
 * Copyright 2021 Karlsruhe Institute of Technology.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.kit.metadatahub.doip.mapping;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import edu.kit.metadatahub.doip.ExtendedOperations;
import edu.kit.metadatahub.doip.handle.IHandleManager;
import edu.kit.metadatahub.doip.handle.impl.HandleMockup;
import edu.kit.metadatahub.doip.mapping.metadata.IMetadataMapper;
import edu.kit.metadatahub.doip.rest.RestDoip;
import edu.kit.metadatahub.doip.server.util.DoipUtil;
import edu.kit.rest.util.SimpleServiceClient;
import edu.kit.turntable.mapping.Creator;
import edu.kit.turntable.mapping.Datacite43Schema;
import edu.kit.turntable.mapping.Date;
import edu.kit.turntable.mapping.HttpCall;
import edu.kit.turntable.mapping.HttpMapping;
import edu.kit.turntable.mapping.Identifier;
import edu.kit.turntable.mapping.Pid;
import edu.kit.turntable.mapping.Title;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.dona.doip.DoipConstants;
import net.dona.doip.client.DigitalObject;
import net.dona.doip.client.DoipException;
import net.dona.doip.client.Element;
import net.dona.doip.server.DoipServerRequest;
import net.dona.doip.server.DoipServerResponse;
import net.dona.doip.util.GsonUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.server.ResponseStatusException;

/**
 * Generic Mapping service from DOIP to HTTP. For adaptions to an existing
 * service the mapping file has to be used.
 *
 */
public class Mapping2HttpService implements IMappingInterface {

  /**
   * Logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(Mapping2HttpService.class);

  private static final String ATTRIBUTE_ALL_ELEMENTS = "includeElementData";
  private static final String ATTRIBUTE_ELEMENT = "element";

  HttpMapping mappingSchema;

  IHandleManager handleManager;

  @Override
  public void initMapping(HttpMapping mapping) {
    mappingSchema = mapping;
    LOGGER.trace("Initialise mapping service with mapping: '{}'", mappingSchema);
    handleManager = new HandleMockup();
  }

  @Override
  public JsonArray listOperationsForService() {
    JsonArray operationsForService = new JsonArray();

    LOGGER.debug("Building list of operations.");
    operationsForService.add(DoipConstants.OP_HELLO);
    operationsForService.add(DoipConstants.OP_LIST_OPERATIONS);
    if ((mappingSchema.getMappings().getDoipOpCreate() != null) && (mappingSchema.getMappings().getDoipOpCreate().getRequestUrl() != null)) {
      operationsForService.add(DoipConstants.OP_CREATE);
    }
    if ((mappingSchema.getMappings().getDoipOpUpdate() != null) && (mappingSchema.getMappings().getDoipOpUpdate().getRequestUrl() != null)) {
      operationsForService.add(DoipConstants.OP_UPDATE);
    }
    if ((mappingSchema.getMappings().getDoipOpRetrieve() != null) && (!mappingSchema.getMappings().getDoipOpRetrieve().isEmpty())) {
      operationsForService.add(DoipConstants.OP_RETRIEVE);
    }
    if ((mappingSchema.getMappings().getDoipOpSearch() != null) && (mappingSchema.getMappings().getDoipOpSearch().getRequestUrl() != null)) {
      operationsForService.add(DoipConstants.OP_SEARCH);
    }
    if ((mappingSchema.getMappings().getDoipOpValidate() != null) && (mappingSchema.getMappings().getDoipOpValidate().getRequestUrl() != null)) {
      operationsForService.add(ExtendedOperations.OP_VALIDATE);
    }
    return operationsForService;
  }

  @Override
  public void listOperationsForService(DoipServerRequest req, DoipServerResponse resp) throws DoipException, IOException {
    JsonArray res = listOperationsForService();
    if (resp != null) {
      LOGGER.debug("Writing list of operations to output.");
      resp.writeCompactOutput(res);
    }
    LOGGER.debug("Returning from listOperationsForService().");
  }

  @Override
  public void create(DoipServerRequest req, DoipServerResponse resp) throws DoipException, IOException {
    LOGGER.debug("Repo: Create...");
    doRestCall(req, resp, mappingSchema.getMappings().getDoipOpCreate());
    resp.setAttribute(DoipConstants.MESSAGE_ATT, "Successfully created!");
    LOGGER.trace("Returning from create().");
  }

  @Override
  public RestDoip createViaRest(DoipUtil restDoip) throws DoipException, IOException {
    RestDoip returnValue = new RestDoip();
    LOGGER.debug("Repo: Create via REST...");
    DigitalObject collectDigitalObject = new DigitalObject();
    collectDigitalObject.elements = new ArrayList<>();
    HttpStatus resource = doPartialRestCall(restDoip, collectDigitalObject, mappingSchema.getMappings().getDoipOpCreate());
    evaluateHttpStatus(resource);
    LOGGER.trace("Writing DigitalObject to output message.");
    DoipUtil.addDigitalObject(returnValue, collectDigitalObject);
    LOGGER.trace("Returning from createViaRest().");
    return returnValue;
  }

  @Override
  public void search(DoipServerRequest req, DoipServerResponse resp) throws DoipException, IOException {
    LOGGER.debug("Repo: Search...");
    doRestCall(req, resp, mappingSchema.getMappings().getDoipOpSearch());
    resp.setAttribute(DoipConstants.MESSAGE_ATT, "Successfully created!");
    LOGGER.trace("Returning from search().");
  }

  @Override
  public RestDoip searchViaRest(DoipUtil restDoip) throws DoipException, IOException {
    RestDoip returnValue = new RestDoip();
    LOGGER.debug("Repo: Search via REST...");
    DigitalObject collectDigitalObject = new DigitalObject();
    LOGGER.error("DigitalObject.id = '{}'", restDoip.getDigitalObject().id);
    collectDigitalObject.id = restDoip.getTargetId();
    collectDigitalObject.elements = new ArrayList<>();
    HttpStatus resource = doPartialRestCall(restDoip, collectDigitalObject, mappingSchema.getMappings().getDoipOpSearch());
    evaluateHttpStatus(resource);

    LOGGER.trace("Writing DigitalObject to output message.");
    DoipUtil.addDigitalObject(returnValue, collectDigitalObject);
    LOGGER.trace("Returning from searchViaRest().");
    return returnValue;
  }

  @Override
  public void validate(DoipServerRequest req, DoipServerResponse resp) throws DoipException, IOException {
    throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
  }

  @Override
  public RestDoip validateViaRest(DoipUtil restDoipS) throws DoipException, IOException {
    throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
  }

  @Override
  public void retrieve(DoipServerRequest req, DoipServerResponse resp) throws DoipException, IOException {
    LOGGER.debug("Repo: Retrieve...");
    boolean retrieveElementOnly = false;
    // Check for correct syntax...
    DoipUtil doipUtil = new DoipUtil(req);
    if (doipUtil.getDigitalObject() != null) {
      LOGGER.error("First segment is not null!?");
      resp.setStatus(DoipConstants.STATUS_BAD_REQUEST);
      resp.setAttribute(DoipConstants.MESSAGE_ATT, "Input is not allowed for retrieving a digital object!");
      throw new DoipException(DoipConstants.MESSAGE_ATT, "Input is not allowed for retrieving a digital object!");
    }
    Set<String> elementSet = new HashSet<>();
    for (HttpCall call : mappingSchema.getMappings().getDoipOpRetrieve()) {
      elementSet.add(call.getLabel());
    }
    String[] allElements = elementSet.toArray(new String[1]);
    String[] selectedElements = new String[1];
    selectedElements[0] = "metadata";
    boolean retrieveNoElements = true;
    if (req.getAttribute(ATTRIBUTE_ALL_ELEMENTS) != null && req.getAttribute(ATTRIBUTE_ALL_ELEMENTS).getAsBoolean()) {
      selectedElements = allElements;
      retrieveNoElements = false;
    }
    if (req.getAttribute(ATTRIBUTE_ELEMENT) != null) {
      retrieveElementOnly = true;
      selectedElements = new String[1];
      selectedElements[0] = req.getAttributeAsString(ATTRIBUTE_ELEMENT);
      retrieveNoElements = false;
    }
    // Collect all elements.
    List<HttpCall> httpCall = new ArrayList<>();
    for (String element : selectedElements) {
      for (HttpCall singleCall : mappingSchema.getMappings().getDoipOpRetrieve()) {
        if (singleCall.getLabel().equals(element)) {
          httpCall.add(singleCall);
          break;
        }
      }
    }
    // Fetch all elements 
    DigitalObject collectDigitalObject = new DigitalObject();
    collectDigitalObject.elements = new ArrayList<>();
    for (HttpCall restCall : httpCall) {
      HttpStatus resource = doPartialRestCall(doipUtil, collectDigitalObject, restCall);
    }
    JsonElement digitalObjectAsJson = GsonUtility.getGson().toJsonTree(collectDigitalObject);
    LOGGER.debug("JSON element: '{}'", digitalObjectAsJson.toString());
    if (retrieveElementOnly) {
      LOGGER.trace("Write element directly to output...");
      if (!collectDigitalObject.elements.isEmpty()) {
        resp.getOutput().writeBytes(collectDigitalObject.elements.get(0).in);
      } else {
        resp.getOutput().writeJson("{}");
      }
    } else {
      resp.getOutput().writeJson(digitalObjectAsJson);
      // attach elements
      if (!retrieveNoElements) {
        for (Element singleElement : collectDigitalObject.elements) {
          writeElementToOutput(resp, singleElement);
        }
      } else {
        collectDigitalObject.elements = new ArrayList<>();
      }
    }
    resp.setStatus(DoipConstants.STATUS_OK);
    resp.setAttribute(DoipConstants.MESSAGE_ATT, "Successfully submitted!");
    resp.getOutput().close();
    resp.commit();
    LOGGER.debug("Finished retrieve!");
  }

  @Override
  public RestDoip retrieveViaRest(DoipUtil restDoip) throws DoipException, IOException {
    RestDoip returnValue = new RestDoip();
    List<HttpCall> httpCall = new ArrayList<>();
    LOGGER.debug("Repo: Retrieve via REST...");
    for (String id : restDoip.getStreams().keySet()) {
      LOGGER.trace("Looking for id '{}'...", id);
      for (HttpCall singleCall : mappingSchema.getMappings().getDoipOpRetrieve()) {
        if (singleCall.getLabel().equals(id)) {
          httpCall.add(singleCall);
          LOGGER.trace("Match! -> Collect id '{}", id);
          break;
        }
      }
    }

    // Fetch all found elements 
    for (HttpCall restCall : httpCall) {
      LOGGER.trace("Fetch id '{}' ...", restCall.getLabel());
      DigitalObject collectDigitalObject = new DigitalObject();
      collectDigitalObject.elements = new ArrayList<>();
      HttpStatus resource = doPartialRestCall(restDoip, collectDigitalObject, restCall);
      evaluateHttpStatus(resource);
      LOGGER.trace("Writing DigitalObject to output message.");
      returnValue = DoipUtil.addDigitalObject(returnValue, collectDigitalObject);
    }
    LOGGER.trace("Returning from retrieveViaRest().");
    return returnValue;
  }

  @Override
  public void update(DoipServerRequest req, DoipServerResponse resp) throws DoipException, IOException {
    LOGGER.debug("Repo: Update...");

    doRestCall(req, resp, mappingSchema.getMappings().getDoipOpUpdate());
    LOGGER.trace("Returning from update().");
  }

  @Override
  public RestDoip updateViaRest(DoipUtil restDoip) throws DoipException, IOException {
    RestDoip returnValue = new RestDoip();
    LOGGER.debug("Repo: Update via REST...");
    DigitalObject collectDigitalObject = new DigitalObject();
    collectDigitalObject.elements = new ArrayList<>();
    HttpStatus resource = doPartialRestCall(restDoip, collectDigitalObject, mappingSchema.getMappings().getDoipOpUpdate());
    evaluateHttpStatus(resource);
    LOGGER.trace("Writing DigitalObject to output message.");
    DoipUtil.addDigitalObject(returnValue, collectDigitalObject);
    LOGGER.trace("Returning from updateViaRest().");
    return returnValue;
  }

  @Override
  public void delete(DoipServerRequest req, DoipServerResponse resp) throws DoipException, IOException {
    throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
  }

  @Override
  public void listOperationsForObject(String targetId, DoipServerRequest req, DoipServerResponse resp) throws DoipException, IOException {
    throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
  }

  private void writeElementToOutput(DoipServerResponse resp, Element element) throws IOException {
    byte[] elementContent = element.in.readAllBytes();
    if (element.in != null) {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Add stream for Element: '{}'", element.id);
        LOGGER.debug("No of Bytes: " + elementContent.length);
        LOGGER.debug("Content: " + new String(elementContent));
      }
      JsonObject json = new JsonObject();
      json.addProperty("id", element.id);
      resp.getOutput().writeJson(json);
      resp.getOutput().writeBytes(elementContent);
    }

  }

  /**
   * Make REST call based on defined mapping.
   *
   * @param mapping
   * @return
   */
  private RestDoip doRestCall(DoipUtil doipUtil, String targetId, HttpCall... mapping) throws DoipException, IOException {
    HttpStatus resource = null;
    RestDoip returnValue = new RestDoip();
    LOGGER.debug("Repo: do REST call via REST ...");
    for (HttpCall singleMapping : mapping) {
      DigitalObject collectDigitalObject = new DigitalObject();
      resource = doPartialRestCall(doipUtil, collectDigitalObject, singleMapping);
      evaluateHttpStatus(resource);
      LOGGER.trace("Writing DigitalObject to output message.");
      DoipUtil.addDigitalObject(returnValue, collectDigitalObject);
    }
    LOGGER.trace("Returning from do REST call.");
    return returnValue;
  }

  /**
   * Make REST call based on defined mapping.
   *
   * @param mapping
   * @return
   */
  private HttpStatus doRestCall(DoipServerRequest req, DoipServerResponse resp, HttpCall... mapping) throws DoipException, IOException {
    HttpStatus resource = null;
    LOGGER.debug("Repo: do REST call ...");
    DoipUtil doipUtil = new DoipUtil(req);
    DigitalObject collectDigitalObject = new DigitalObject();
    for (HttpCall singleMapping : mapping) {
      resource = doPartialRestCall(doipUtil, collectDigitalObject, singleMapping);
      evaluateHttpStatus(resource, resp);
      JsonElement dobjJson = GsonUtility.getGson().toJsonTree(collectDigitalObject);
      LOGGER.trace("Writing DigitalObject to output message.");
      resp.writeCompactOutput(dobjJson);
      resp.setStatus(DoipConstants.STATUS_OK);
    }
    LOGGER.trace("Returning from do REST call.");
    return resource;
  }

  /**
   * Make REST call based on defined mapping.
   *
   * @param mapping
   * @return
   */
  private HttpStatus doPartialRestCall(DoipUtil doipUtil, DigitalObject collectDigitalObject, HttpCall mapping) throws DoipException, IOException {
    HttpStatus resource = null;
    Map<String, String> replaceMapping = new HashMap<>();
    LOGGER.debug("Repo: prepare REST call ...");
    // First of all get targetId.
    String targetId = doipUtil.getTargetId();
//    String digitalObjectId = doipUtil.getDigitalObject().id;
    replaceMapping.put("targetId", targetId);
    for (String key : doipUtil.getStreams().keySet()) {
      LOGGER.trace("Found key in DO: '{}'", key);
      if (!key.equals("metadata")
              && (mapping.getBody() != null)
              && (mapping.getBody().getAdditionalProperties().keySet().contains(key))) {
        replaceMapping.put(key, new String(doipUtil.getStreams().get(key)));
        LOGGER.trace("Add to mapping: '{}' -> '{}'", key, replaceMapping.get(key));
      }
    }
    if (LOGGER.isTraceEnabled()) {
      for (String key : replaceMapping.keySet()) {
        LOGGER.warn("MAPPING '{}' --> '{}'", key, replaceMapping.get(key));
      }
    }
    DigitalObject digitalObject = doipUtil.getDigitalObject();
    Map<String, byte[]> streamMap = doipUtil.getStreams();
    Datacite43Schema datacite = doipUtil.getDatacite();
    // There should be an implementation class inside the mapping...
//    SchemaRecordSchema metadata = metadataMapper.mapFromDatacite(datacite);
    // for Metastore handle is created outside (yet)
    Pid pid = new Pid();
    pid.setIdentifier(handleManager.createHandle());
    pid.setIdentifierType("HANDLE");
//    metadata.setPid(pid);

    String baseUrl = mapping.getRequestUrl();//"http://localhost:8040/api/v1/schemas";
    if (baseUrl != null) {
      baseUrl = replacePlaceholders(baseUrl, replaceMapping);
      LOGGER.trace("baseURL: '{}'", baseUrl);
      String acceptType = mapping.getMimetype(); //"application/json";

      SimpleServiceClient simpleClient = SimpleServiceClient.create(baseUrl);
      simpleClient.accept(MediaType.parseMediaType(acceptType));
      // Add authentication if available
      if (doipUtil.getAuthentication() != null) {
        JsonElement authentication = doipUtil.getAuthentication();
        if (authentication.isJsonObject()) {
          LOGGER.trace("Authentication available: " + authentication.toString());
          JsonObject object = (JsonObject) authentication;
          if (object.has("token")) {
            simpleClient.withBearerToken(object.get("token").getAsString());
          } else {
            LOGGER.warn("Only authorization via token supported yet!");
          }
        }
      }
      ///////////////////////////////////////////////////////////////
      // Prepare metadata
      ///////////////////////////////////////////////////////////////
      IMetadataMapper metadataMapper = null;
      Object metadata = datacite;
      if (mapping.getMetadata() != null) {
        try {
          metadataMapper = (IMetadataMapper) Class.forName(mapping.getMetadata().getMapperClass()).getDeclaredConstructor().newInstance();
          // There should be an implementation class inside the mapping...
          metadata = metadataMapper.mapFromDatacite(datacite);
          LOGGER.trace("Transformed datacite metadata to '{}'.", metadata.getClass());
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InstantiationException | SecurityException | InvocationTargetException ex) {
          LOGGER.error(null, ex);
        }
      }
      Class<?> metadataClassResponse = Datacite43Schema.class;
      IMetadataMapper metadataMapperResponse = null;
      if ((mapping.getResponse() != null)
              && (mapping.getResponse().getClassName() != null)) {
        LOGGER.trace("Get mapper for '{}'", mapping.getResponse().getMapperClass());
        LOGGER.trace("Get class for '{}'", mapping.getResponse().getClassName());
        try {
          metadataClassResponse = Class.forName(mapping.getResponse().getClassName());
          metadataMapperResponse = (IMetadataMapper) Class.forName(mapping.getResponse().getMapperClass()).getDeclaredConstructor().newInstance();
        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
          LOGGER.error(null, ex);
        }
      }
      ///////////////////////////////////////////////////////////////
      // Prepare headers
      // header with values will be assigned to header send.
      // header without values will be assigned to collect headers.
      ///////////////////////////////////////////////////////////////
      Map<String, String> container = new HashMap<>();
      if (mapping.getHeader() != null) {
        for (String attr : mapping.getHeader().getAdditionalProperties().keySet()) {
          LOGGER.trace("Add header: '{}'", attr);
          String value = null;
          if ((digitalObject != null)
                  && (digitalObject.attributes != null)
                  && (digitalObject.attributes.getAsJsonObject("header") != null)
                  && (digitalObject.attributes.getAsJsonObject("header").get(attr) != null)
                  && (!digitalObject.attributes.getAsJsonObject("header").get(attr).isJsonNull())) {
            value = digitalObject.attributes.getAsJsonObject("header").get(attr).getAsString();
            if (value != null) {
              LOGGER.trace("Add header: '{}'= '{}'", attr, value);
              simpleClient.withHeader(attr, value);
            }
          }
          LOGGER.trace("Add key for collecting header: '{}'", attr);
          container.put(attr, value);
        }
        simpleClient.collectResponseHeader(container);
      }
      ///////////////////////////////////////////////////////////////
      // Prepare request
      ///////////////////////////////////////////////////////////////
      String mappingContentType = mapping.getContenttype() == null ? MediaType.MULTIPART_FORM_DATA_VALUE : mapping.getContenttype();
      LOGGER.trace("Content-Type: '{}'", mappingContentType);
      switch (mapping.getVerb()) {
        case "GET":
          collectDigitalObject.id = doipUtil.getTargetId();
          break;
        case "POST":
        case "PUT":
          if (mappingContentType.equals(MediaType.MULTIPART_FORM_DATA_VALUE)) {
            // add entries to form
            for (String key : streamMap.keySet()) {
              LOGGER.trace("Found stream: '{}' -> '{}'", key, streamMap.get(key).length);
              String mappingKey = key;
              if (mapping.getBody().getAdditionalProperties().containsKey(key)) {
                mappingKey = mapping.getBody().getAdditionalProperties().get(key);
              }
              InputStream documentStream = new ByteArrayInputStream(streamMap.get(key));
              simpleClient.withFormParam(mappingKey, documentStream);
            }
            String mappingKey = DoipUtil.ID_METADATA;
            if (mapping.getBody() != null) {
              if (mapping.getBody().getMetadata() != null) {
                mappingKey = mapping.getBody().getMetadata();
              }
              simpleClient.withFormParam(mappingKey, metadata);
            }
          }
          break;
        default:
          throw new DoipException(DoipConstants.STATUS_BAD_REQUEST, "Mapping verb is not correct!");

      }
      ///////////////////////////////////////////////////////////////
      // Make request
      ///////////////////////////////////////////////////////////////
      Object responseBody = null;
      switch (mapping.getVerb()) {
        case "GET":
          if (metadataMapperResponse != null) {
            responseBody = simpleClient.getResource(metadataClassResponse);
            if (!(responseBody instanceof Datacite43Schema)) {
              datacite = ((IMetadataMapper) metadataMapperResponse).mapToDatacite(responseBody);
            }
            responseBody = datacite;
          } else {
            responseBody = simpleClient.getResource(String.class);
          }
          resource = simpleClient.getResponseStatus();

          break;
        case "POST":
          if (mappingContentType.equals(MediaType.MULTIPART_FORM_DATA_VALUE)) {
            // post form and get HTTP status
            resource = simpleClient.postForm(); //Resource(srs, SchemaRecordSchema.class);
          } else {
            for (String key : mapping.getBody().getAdditionalProperties().keySet()) {
              // in mapper empty now defined. Don't use this. It's only a string which
              // should be replaced in other fields.
              LOGGER.trace("Key defined in mapping: '{}' length = '{}'", key, mapping.getBody().getAdditionalProperties().get(key).length());
              if (!mapping.getBody().getAdditionalProperties().get(key).isBlank()) {
                String actualValue = streamMap.get(key) != null ? new String(streamMap.get(key)) : mapping.getBody().getAdditionalProperties().get(key);
                if (LOGGER.isTraceEnabled()) {
                  for (String mappingKey : replaceMapping.keySet()) {
                    LOGGER.warn("MAPPING '{}' --> '{}'", mappingKey, replaceMapping.get(mappingKey));
                  }
                }
                String fixedString = replacePlaceholders(actualValue, replaceMapping);
                LOGGER.trace("Substitute string: '{}' -> '{}'", actualValue, fixedString);
                LOGGER.trace("... and send request.");
                simpleClient.withContentType(MediaType.parseMediaType(mappingContentType));
                responseBody = simpleClient.postResource(fixedString, String.class);
                LOGGER.trace("Get response: '{}'", responseBody);
                resource = simpleClient.getResponseStatus();
                LOGGER.trace("Get response status: '{}'", resource);
                break;
              }
            }

//            for (String key : streamMap.keySet()) {
//              LOGGER.trace("Found stream: '{}' -> '{}'", key, streamMap.get(key).length);
//              String mappingKey = key;
//              LOGGER.trace("Keys found in body: '{}'", mapping.getBody().getAdditionalProperties().keySet());
//              if (mapping.getBody().getAdditionalProperties().containsKey(key)) {
//                if (LOGGER.isTraceEnabled()) {
//                  mappingKey = mapping.getBody().getAdditionalProperties().get(key);
//                  String documentStream = new String(streamMap.get(key));
//                  LOGGER.trace("Document: " + documentStream);
//                }
//                resource = simpleClient.postForm(MediaType.parseMediaType(mappingContentType));
//                responseBody = simpleClient.getResponseBody();
//                System.out.println("Volker: *******************" + responseBody);
//                break;
//              }
//            }
          }
          break;
        case "PUT":
          if (mappingContentType.equals(MediaType.MULTIPART_FORM_DATA_VALUE)) {
            resource = simpleClient.putForm(); //Resource(srs, SchemaRecordSchema.class);
          } else {
            for (String key : streamMap.keySet()) {
              LOGGER.trace("Found stream: '{}' -> '{}'", key, streamMap.get(key).length);
              String mappingKey = key;
              LOGGER.trace("Keys found in body: '{}'", mapping.getBody().getAdditionalProperties().keySet());
              if (mapping.getBody().getAdditionalProperties().containsKey(key)) {
                mappingKey = mapping.getBody().getAdditionalProperties().get(key);
                String documentStream = new String(streamMap.get(key));
                System.out.println("Document: " + documentStream);
                simpleClient.putResource(documentStream, String.class);
                resource = simpleClient.getResponseStatus();
                break;
              }
            }
          }
          break;
      }

      ///////////////////////////////////////////////////////////////
      // Collect response
      ///////////////////////////////////////////////////////////////
      if ((resource != null) && resource.is2xxSuccessful() && resource != resource.NO_CONTENT) {
        // get response as object
        if (responseBody == null) {
          responseBody = simpleClient.getResponseBody(metadataClassResponse);
          if (metadataMapperResponse != null) {
            datacite = ((IMetadataMapper) metadataMapperResponse).mapToDatacite(responseBody);
          }
          responseBody = (Datacite43Schema) datacite;
        }
        LOGGER.trace("Build response...");
        if (collectDigitalObject.id == null) {
          collectDigitalObject.id = datacite.getIdentifiers().iterator().next().getIdentifier();
        }
        if (collectDigitalObject.attributes == null) {
          collectDigitalObject.attributes = new JsonObject();
        }
        collectDigitalObject.attributes.add(DoipUtil.ATTR_DATACITE, GsonUtility.getGson().toJsonTree(datacite));
        collectDigitalObject.type = DoipUtil.TYPE_DO;
        if (collectDigitalObject.elements == null) {
          collectDigitalObject.elements = new ArrayList<>();
        } //= digitalObject.elements;
        switch (mapping.getVerb()) {
          case "POST":
          case "GET":
            LOGGER.trace("Add element '{}' to digital object. ", mapping.getLabel());
            // Add response to response object
            Element doipElement = new Element();
            doipElement.id = mapping.getLabel();
            if (responseBody instanceof String) {
              doipElement.in = new ByteArrayInputStream(((String) responseBody).getBytes());
            } else {
              JsonElement jsonElement = GsonUtility.getGson().toJsonTree(responseBody);
              LOGGER.trace("Writing DigitalObject to output message.");
              doipElement.in = new ByteArrayInputStream(jsonElement.toString().getBytes());
            }
            collectDigitalObject.elements.add(doipElement);
            break;
          default:
        }
        JsonObject restHeader = new JsonObject();
        if (collectDigitalObject.attributes.get("header") != null) {
          restHeader = collectDigitalObject.attributes.getAsJsonObject("header");
        }
        for (String items : container.keySet()) {
          restHeader.addProperty(items, container.get(items));
        }
        collectDigitalObject.attributes.add("header", restHeader);
      }
      LOGGER.trace("Returning digital object from REST call.");
    } else {
      LOGGER.trace("No baseURL defined! Build datacite from request.");
      LOGGER.trace("mapping '{}'", mapping);
      LOGGER.trace("mapping.response '{}'", mapping.getResponse());
      Element doipElement = new Element();
      doipElement.id = mapping.getLabel();
      Map<String, Object> additionalProperties = mapping.getResponse().getAdditionalProperties();
      if (LOGGER.isTraceEnabled()) {
        LOGGER.trace("Additional properties for response: ");
        LOGGER.trace("#########################################################");
        for (String property : additionalProperties.keySet()) {
          LOGGER.trace("'{}' -> '{}'", property, additionalProperties.get(property));
        }
        LOGGER.trace("#########################################################");
      }
      Datacite43Schema responseBody = createDatacite(additionalProperties, replaceMapping);
      JsonElement jsonElement = GsonUtility.getGson().toJsonTree(responseBody);
      LOGGER.trace("Writing DigitalObject to output message.");
      doipElement.in = new ByteArrayInputStream(jsonElement.toString().getBytes());
      collectDigitalObject.elements.add(doipElement);
    }
    return resource;
  }

  private void evaluateHttpStatus(HttpStatus httpStatus, DoipServerResponse resp) throws DoipException {
    if ((httpStatus == null) || !httpStatus.is2xxSuccessful()) {
      // do some error handling
      String status = null;
      switch (httpStatus) {
        case BAD_REQUEST:
          status = DoipConstants.STATUS_BAD_REQUEST;
          break;
        case UNAUTHORIZED:
          status = DoipConstants.STATUS_UNAUTHENTICATED;
          break;
        case CONFLICT:
          status = DoipConstants.STATUS_CONFLICT;
          break;
        case FORBIDDEN:
          status = DoipConstants.STATUS_FORBIDDEN;
          break;
        case NOT_FOUND:
          status = DoipConstants.STATUS_NOT_FOUND;
          break;
        default:
          status = DoipConstants.STATUS_ERROR;
      }
      resp.setStatus(status);
      resp.setAttribute(DoipConstants.MESSAGE_ATT, httpStatus.getReasonPhrase());
      throw new DoipException(DoipConstants.MESSAGE_ATT, httpStatus.getReasonPhrase());
    }
  }

  private void evaluateHttpStatus(HttpStatus httpStatus) throws ResponseStatusException {
    if ((httpStatus == null) || !httpStatus.is2xxSuccessful()) {
      // do some error handling
      throw new ResponseStatusException(httpStatus, httpStatus.getReasonPhrase(), null);
    }
  }

  /**
   * Create datacite object from request.
   *
   * @param metadataObject
   * @param placeholders
   * @return datacite object.
   */
  public Datacite43Schema createDatacite(Map<String, Object> mappings, Map<String, String> placeholders) {
    Datacite43Schema datacite = new Datacite43Schema();
    datacite.getFormats().add(replacePlaceholders(mappings.getOrDefault("format", "application/json").toString(), placeholders));
    Title title = new Title();
    title.setTitle(replacePlaceholders(mappings.getOrDefault("title", "No title!").toString(), placeholders));
    title.setTitleType(Title.TitleType.OTHER);
    datacite.getTitles().add(title);
    Date dates = new Date();
    dates.setDate(new Date().toString());
    dates.setDateType(Date.DateType.OTHER);
    datacite.getDates().add(dates);
    Identifier identifier = new Identifier();
    identifier.setIdentifier(replacePlaceholders(mappings.getOrDefault("identifier", "no identifier").toString(), placeholders));
    identifier.setIdentifierType(replacePlaceholders(mappings.getOrDefault("identifierType", "OTHER").toString(), placeholders));
    datacite.getIdentifiers().add(identifier);
    Creator creator = new Creator();
    creator.setName(replacePlaceholders(mappings.getOrDefault("creator", "UNKNOWN").toString(), placeholders));
    datacite.getCreators().add(creator);
    datacite.setPublisher(replacePlaceholders(mappings.getOrDefault("publisher", "UNKNOWN").toString(), placeholders));

    return datacite;
  }

  /**
   * Replace placeholders by values defined in map.
   *
   * @param originalString String containing placeholders.
   * @param placeholders Map containing all possible placeholders with new value
   * @return String with values defined in map.
   */
//  public String replacePlaceholders(String originalString, Map<String, String> placeholders) {
//    String patchedString = originalString;
//    if (originalString.contains("{")) {
//      // There might be more elegant ways to do (e.g. get all contained keys via regexp)
//      for (String placeholder : placeholders.keySet()) {
//        String newValue = placeholders.get(placeholder);
//        LOGGER.trace("'{}' --> '{}'", placeholder, newValue);
//        patchedString = patchedString.replace("{url_encode(" + placeholder + ")}", URLEncoder.encode(newValue, Charset.forName("UTF-8")));
//        patchedString = patchedString.replace("{" + placeholder + "}", newValue);
//      }
//    }
//    return patchedString;
//  }
  /**
   * Replace placeholders by values defined in map.
   *
   * @param inputString String containing placeholders.
   * @param replaceMap Map containing all possible placeholders with new value
   * @return String with values defined in map.
   */
  public static String replacePlaceholders(String inputString, Map<String, String> replaceMap) {
    // Placeholder should start with '${' and end with '}' 
    String[] result;
    // Search for ${...}
    result = findPatterns(inputString, "\\{([^{}]*)\\}");
    for (String placeholder : result) {
      try {
        LOGGER.trace("Replace this: '{}' ...", placeholder);
        // Look for function
        String valueForPlaceholder = executeFunction(placeholder, replaceMap);
        LOGGER.trace("...by '{}'.", valueForPlaceholder);
        inputString = inputString.replace("{" + placeholder + "}", valueForPlaceholder);
      } catch (Exception ex) {
        LOGGER.error("Any exception occured during replacing '" + placeholder + "'!", ex);
      }
    }

    return inputString;
  }

  private static String executeFunction(String inputString, Map<String, String> replaceMap) {
    String value = null;
    String function = evaluatePlaceholder(inputString, "([^\\(]*)\\(.*\\)");
    if (function != null) {
      LOGGER.trace("Function: " + function);
      String parameter = evaluatePlaceholder(inputString, "[^\\(]*\\((.*)\\)");
      switch (function) {
        case "url_encode":
          LOGGER.trace("Do url_encode '{}'....", parameter);
          parameter = executeFunction(parameter, replaceMap);
          LOGGER.trace(".... -> '{}'", parameter);

          try {
            value = URLEncoder.encode(parameter, "UTF-8");
          } catch (UnsupportedEncodingException ex) {
            LOGGER.error("Error encoding string", ex);
          }

          break;

        default:
          value = replaceByMap(function, replaceMap);
          value = findPatterns(value, parameter)[0];

      }
    } else {
      // replace with map entry
      value = replaceByMap(inputString, replaceMap);
    }
    LOGGER.trace("Return: " + value);
    return value;
  }

  private static String evaluatePlaceholder(String inputString, String pattern) {
    String value = null;
    Matcher matcher = Pattern.compile(pattern).matcher(inputString);
    int index = 1;
    if (matcher.find()) {
      value = matcher.group(1);
      LOGGER.trace("Matcher[" + index++ + "]: " + value);
    }
    return value;
  }

  private static String[] findPatterns(String inputString, String pattern) {
    List<String> value = new ArrayList<>();
    LOGGER.trace("Looking for pattern '{}' inside '{}'", pattern, inputString);

    Matcher matcher = Pattern.compile(pattern).matcher(inputString);
    int index;
    while (matcher.find()) {
      index = matcher.groupCount();
      String match = matcher.group(index);
      LOGGER.trace("Found placeholder: '{}' of '{}' groups!", match, index);
      value.add(match);
    }

    return value.toArray(String[]::new);
  }

  private static String replaceByMap(String inputString, Map<String, String> replaceMap) {
    String replaceString = replaceMap.get(inputString);
//    for (String key : replaceMap.keySet()) {
//      replaceString = replaceString.replace(key, replaceMap.get(key));
//    }
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("Replace '{}' --> '{}'!", inputString, replaceString);

    }
    return replaceString;
  }
}
