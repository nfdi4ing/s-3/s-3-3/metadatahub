/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package edu.kit.metadatahub.doip.rest;

import org.springframework.http.HttpStatus;

/**
 * Define valid operations for REST interface of DOIP.
  */
public enum DoipResponse {
    STATUS_OK("0.DOIP/Status.001", HttpStatus.OK),
    STATUS_101("0.DOIP/Status.101", HttpStatus.BAD_REQUEST),
    STATUS_200("0.DOIP/Status.200", HttpStatus.BAD_REQUEST),
    STATUS_401("0.DOIP/Status.102", HttpStatus.UNAUTHORIZED),
    STATUS_403("0.DOIP/Status.103", HttpStatus.FORBIDDEN),
    STATUS_404("0.DOIP/Status.104", HttpStatus.NOT_FOUND),
    STATUS_409("0.DOIP/Status.105", HttpStatus.CONFLICT),
    STATUS_500("0.DOIP/Status.500", HttpStatus.INTERNAL_SERVER_ERROR);

    String value;
    
    HttpStatus status;
    
    DoipResponse(String v, HttpStatus status) {
        value = v;
        this.status = status;
    }

    public String value() {
        return value;
    }

    public HttpStatus status() {
        return status;
    }

    public static DoipResponse fromValue(String v) {
        for (DoipResponse c: DoipResponse.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
 
}
