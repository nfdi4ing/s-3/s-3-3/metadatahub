/*
 * Copyright 2021 Karlsruhe Institute of Technology.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.kit.metadatahub.doip.server.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import edu.kit.metadatahub.doip.rest.Content;
import edu.kit.turntable.mapping.Datacite43Schema;
import edu.kit.turntable.mapping.RelatedIdentifier;
import edu.kit.metadatahub.doip.rest.RestDoip;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import net.dona.doip.DoipConstants;
import net.dona.doip.InDoipSegment;
import net.dona.doip.client.DigitalObject;
import net.dona.doip.client.DoipException;
import net.dona.doip.client.Element;
import net.dona.doip.server.DoipServerRequest;
import net.dona.doip.util.GsonUtility;
import net.dona.doip.util.InDoipMessageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class for generating, parsing the data from/to a DOIP object.
 */
public class DoipUtil {

  /**
   * Label of the attribute containing datacite (in JSON format) metadata about
   * the DO.
   */
  public static final String ATTR_DATACITE = "datacite";
  public static final String ATTR_HEADER = "header";
  public static final String ID_SCHEMA = "schema";
  public static final String ID_METADATA = "metadata";
  public static final String ID_APPLICATION_PROFILE = "application_profile";
  /**
   * Label for the general DO type.
   */
  public static final String TYPE_DO = "0.TYPE/DO";

  private static final Logger LOGGER = LoggerFactory.getLogger(DoipUtil.class);

  private DigitalObject digitalObject = null;
  /**
   * Map holding all streams of request.
   */
  private Map<String, byte[]> streamMap = null;
  /**
   * Request.
   */
  private final DoipServerRequest doipServerRequest;
  /**
   * Request via REST.
   */
  private final RestDoip restDoipRequest;
  /**
   * Target ID.
   */
  private final String targetId;

  public DoipUtil(DoipServerRequest req) {
    doipServerRequest = req;
    targetId = req.getTargetId();
    restDoipRequest = null;
  }

  public DoipUtil(RestDoip req) {
    doipServerRequest = null;
    restDoipRequest = req;
    this.targetId = req.getTargetId();
  }

  public DigitalObject getDigitalObject() throws DoipException, IOException {
    if (digitalObject == null) {
      if (doipServerRequest != null) {
        InDoipSegment firstSegment = InDoipMessageUtil.getFirstSegment(doipServerRequest.getInput());
        if (firstSegment != null) {
          if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Deserializing digital object from first segment.");
            LOGGER.trace("**********************************************************************");
            LOGGER.trace("Digital Object: '{}'", firstSegment.getJson());
            LOGGER.trace("**********************************************************************");
          }
          digitalObject = GsonUtility.getGson().fromJson(firstSegment.getJson(), DigitalObject.class);
        }
      } else {
        digitalObject = new DigitalObject();
        digitalObject.id = restDoipRequest.getId();
        digitalObject.type = "0.TYPE/DO";
        digitalObject.attributes = new JsonObject();
        if (restDoipRequest.getDatacite() != null) {
          JsonElement dobjJson = GsonUtility.getGson().toJsonTree(restDoipRequest.getDatacite());
          digitalObject.attributes.add(ATTR_DATACITE, dobjJson);
          if (LOGGER.isTraceEnabled()) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String jsonStr = gson.toJson(dobjJson);
            LOGGER.trace("Datacite: '{}'", jsonStr);
          }
        }
        // Add attributes 
        JsonObject attributes = digitalObject.attributes;
        for (Content attribute : restDoipRequest.getAttributes()) {
          attributes.addProperty(attribute.getId(), attribute.getValue());
        }
        // Add attributes for header
        JsonObject headerAttributes = new JsonObject();
        for (Content attribute : restDoipRequest.getHeader()) {
          headerAttributes.addProperty(attribute.getId(), attribute.getValue());
        }
        attributes.add("header", headerAttributes);

        // Add all elements to digitalObject (DOIP) and add them to streamMap
        streamMap = new HashMap<>();
        LOGGER.trace("Looking for elements inside RestDoip!");
        if (restDoipRequest.getElements() != null && !restDoipRequest.getElements().isEmpty()) {
//          digitalObject.elements = new ArrayList<>();
          for (Content attribute : restDoipRequest.getElements()) {
            LOGGER.trace("Found element with id '{}' and value '{}'!", attribute.getId(), attribute.getValue());
            streamMap.put(attribute.getId(), attribute.getValue().getBytes());
          }
        }
      }
    }
    return digitalObject;
  }

  /**
   * Get Datacite attribute from a digital object.
   *
   * @return Datacite
   * @throws IOException
   */
  public Datacite43Schema getDatacite() throws DoipException, IOException {
    return getDatacite(getDigitalObject());
  }

  /**
   * Get Datacite attribute from a digital object.
   *
   * @param dobj Digital object.
   * @return Datacite
   * @throws IOException
   */
  public Datacite43Schema getDatacite(DigitalObject dobj) throws IOException {
    LOGGER.trace("getDatacite....");
    Datacite43Schema resource = null;
    if (dobj != null) {
      JsonElement metadata = dobj.attributes.get(ATTR_DATACITE);
      if (LOGGER.isTraceEnabled()) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonStr = gson.toJson(metadata);
        LOGGER.trace("-----------------------------------------------------------------");
        LOGGER.trace("Datacite: '{}'", jsonStr);
        LOGGER.trace("-----------------------------------------------------------------");
      }

      // Replace enums by its name to fix problem serializing ENUMS
      // isMetadataFor -> IS_METADATA_FOR
      String metadataString = metadata.toString();
      for (RelatedIdentifier.RelationType type : RelatedIdentifier.RelationType.values()) {
        metadataString = metadataString.replace(addQuotes(type.value()), addQuotes(type.name()));
      }
      LOGGER.trace("-----------------------------------------------------------------");
      LOGGER.trace("Datacite string of digital object: '{}'", metadataString);
      LOGGER.trace("-----------------------------------------------------------------");
      resource = GsonUtility.getGson().fromJson(metadataString, Datacite43Schema.class);
    }
    LOGGER.trace("-----------------------------------------------------------------");
    LOGGER.trace("Datacite of digital object: '{}'", resource);
    LOGGER.trace("-----------------------------------------------------------------");
    return resource;
  }

  public static DigitalObject ofDataResource(Datacite43Schema resource) throws DoipException {
    net.dona.doip.client.DigitalObject digitalObject = new DigitalObject();
    digitalObject.id = resource.getIdentifiers().iterator().next().getIdentifier();
    digitalObject.type = "0.TYPE/DO";
    digitalObject.attributes = new JsonObject();
    JsonElement dobjJson = GsonUtility.getGson().toJsonTree(resource);
    digitalObject.attributes.add(ATTR_DATACITE, dobjJson);
    return digitalObject;
  }

  /**
   * Get boolean attribute of request.
   *
   * @param att Name of attribute.
   * @return attribute.
   */
  public boolean getBooleanAttributeFromRequest(String att) {
    return getBooleanAttributeFromRequest(doipServerRequest, att);
  }

  /**
   * Get boolean attribute of request.
   *
   * @param req Request.
   * @param att Name of attribute.
   * @return attribute.
   */
  public boolean getBooleanAttributeFromRequest(DoipServerRequest req, String att) {
    boolean returnValue = false;
    if (req != null) {
      JsonElement el = req.getAttribute(att);
      if (el == null || !el.isJsonPrimitive()) {
        return false;
      }

      JsonPrimitive priv = el.getAsJsonPrimitive();
      if (priv.isBoolean()) {
        return priv.getAsBoolean();
      }
      if (priv.isString()) {
        returnValue = "true".equalsIgnoreCase(priv.getAsString());
      }
    }
    if (restDoipRequest != null) {
      for (Content attributes : restDoipRequest.getAttributes()) {
        if (attributes.getId().equalsIgnoreCase(att)) {
          returnValue = "true".equalsIgnoreCase(attributes.getValue());
        }
      }
    }
    return returnValue;
  }

  public Map<String, byte[]> getStreams() throws DoipException {
    try {
      // First read the digital object.
      getDigitalObject();
    } catch (IOException ex) {
      LOGGER.error("Can't read digital object!");
    }
    if (streamMap == null) {
      streamMap = new HashMap<>();
      Iterator<InDoipSegment> iterator = doipServerRequest.getInput().iterator();
      while (iterator.hasNext()) {
        InDoipSegment segment = iterator.next();
        if (LOGGER.isTraceEnabled()) {
          LOGGER.trace("*************************************************************");
          LOGGER.trace("Next Segment.....");
          LOGGER.trace("*************************************************************");
        }
        if (segment.isJson() == false) {
          throw new DoipException(DoipConstants.STATUS_BAD_REQUEST, "Segment should be a JSON!");
        } else {
          try {
            // Read first part of segment which should contain JSON.
            // Read id of element
            LOGGER.trace("Content: '{}'", segment.getJson().toString());
            String id = segment.getJson().getAsJsonObject().get("id").getAsString();
            LOGGER.trace("ID: '{}'", id);

            segment = iterator.next();

            streamMap.put(id, segment.getInputStream().readNBytes(65536));
            segment.getInputStream().close();
          } catch (IOException ex) {
            throw new DoipException(DoipConstants.STATUS_BAD_REQUEST, "Error while reading JSON!");
          }
        }
      }
      if (LOGGER.isTraceEnabled()) {
        for (String key : streamMap.keySet()) {
          LOGGER.trace("Found stream: '{}' -> '{}'", key, streamMap.get(key).length);
        }
      }
    }
    return streamMap;
  }

  /**
   * Gets target id of request.
   */
  public String getTargetId() {
    return targetId;
  }

  /**
   * Gets authentication of request.
   */
  public JsonElement getAuthentication() {
    JsonElement returnValue;
    if (doipServerRequest != null) {
      returnValue = doipServerRequest.getAuthentication();
    } else {
      JsonObject auth = new JsonObject();
      auth.addProperty("token", restDoipRequest.getToken());
      returnValue = auth;
    }
    return returnValue;
  }

  public static RestDoip addDigitalObject(RestDoip base, DigitalObject addObject) {
    LOGGER.trace("Add digital object to RestDoip....");
    base.setId(addObject.id);
    if (addObject.attributes != null) {
      if (addObject.attributes.get(ATTR_DATACITE) != null && !addObject.attributes.get(ATTR_DATACITE).isJsonNull()) {
        LOGGER.trace("Add datacite to RestDoip....");
        JsonElement datacite = addObject.attributes.get(ATTR_DATACITE);
        Gson gson = new Gson();
        Datacite43Schema dc = gson.fromJson(datacite, Datacite43Schema.class);
        base.setDatacite(dc);
      }
      if (addObject.attributes.get(ATTR_HEADER) != null && !addObject.attributes.get(ATTR_HEADER).isJsonNull()) {
        JsonObject header = addObject.attributes.getAsJsonObject(ATTR_HEADER);
        List<Content> headerElements = base.getHeader();
        if (headerElements == null) {
          headerElements = new ArrayList<>();
        }
        for (String key : header.keySet()) {
          if (!header.get(key).isJsonNull()) {
            LOGGER.trace("Add header '{}' to RestDoip....", key);
            Content content = new Content();
            content.setId(key);
            content.setValue(header.get(key).getAsString());
            headerElements.add(content);
          }
        }
        base.setHeader(headerElements);
      }
      List<Content> attributes = base.getAttributes();
      if (attributes == null) {
        attributes = new ArrayList<>();
      }
      for (String key : addObject.attributes.keySet()) {
        if (!key.equalsIgnoreCase(ATTR_DATACITE) && !key.equalsIgnoreCase(ATTR_HEADER)) {
          Content content = new Content();
          content.setId(key);
          content.setValue(addObject.attributes.get(key).getAsString());
          attributes.add(content);
        }
      }
      base.setAttributes(attributes);
    }
    List<Content> elements = base.getElements();
    if (elements == null) {
      elements = new ArrayList<>();
    }
    if (addObject.elements != null) {
      for (Element element : addObject.elements) {
        Content newElement = new Content();
        newElement.setId(element.id);
        newElement.setType(element.type);
        String value = new BufferedReader(
                new InputStreamReader(element.in, StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.joining("\n"));

        newElement.setValue(value);
        elements.add(newElement);
      }
      base.setElements(elements);
    }
    if (LOGGER.isTraceEnabled()) {
      Gson gson = new GsonBuilder().setPrettyPrinting().create();
      String jsonStr = gson.toJson(base);
      LOGGER.trace("---------------------------------------------------------------------");
      LOGGER.trace(jsonStr);
      LOGGER.trace("---------------------------------------------------------------------");
    }
    return base;
  }
  /**
   * Add quotes to a string to avoid partly replacement for values inside a
   * json document.
   * @param value value without quotes.
   * @return value with quotes.
   */
  public static String addQuotes(String value) {
    String returnValue = value;
    String quote = "\"";
    if (!value.startsWith(quote) && !value.endsWith(quote)) {
      returnValue = quote + value + quote;
    }
    return returnValue;
  }
  
}
