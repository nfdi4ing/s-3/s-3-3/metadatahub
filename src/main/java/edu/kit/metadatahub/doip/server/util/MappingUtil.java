/*
 * Copyright 2021 Karlsruhe Institute of Technology.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.kit.metadatahub.doip.server.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import edu.kit.metadatahub.rest.controller.MappingController;
import edu.kit.turntable.mapping.HttpMapping;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class for managing mappings for repositories.
 */
public class MappingUtil {

  /**
   * Logger.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(MappingUtil.class);
  /**
   * Default folder for mappings.
   */
  public static String MAPPINGS_DIR_DEFAULT = "mappings";
  /**
   * Default suffix for mappings.
   */
  public static String MAPPINGS_SUFFIX_DEFAULT = "_mappings.json";
  /**
   * Mapping which should be used if no mapping is given.
   */
  public static final String DEFAULT_MAPPING = "default";

  /**
   * Map holding all mappings.
   */
  private static Map<String, HttpMapping> allMappings;
  /**
   * Directory where to look for mappings.
   */
  private String mappingsDir;
  /**
   * Suffix of valid mapping files.
   */
  private String mappingsSuffix;
  /**
   * Constructor with default settings for mappingsDir and mappingsSuffix.
   */
  public MappingUtil() {
    this(MAPPINGS_DIR_DEFAULT, MAPPINGS_SUFFIX_DEFAULT);
  }
  /**
   * Constructor with new settings.
   * @param mappingsDir Directory where all mappings are stored.
   * @param mappingsSuffix Suffix of allowed mappings. (e.g.: '_mappings.json')
   */
  public MappingUtil(String mappingsDir, String mappingsSuffix) {
    this.mappingsDir = mappingsDir;
    this.mappingsSuffix = mappingsSuffix;
    parseAllMappings();
  }

  /**
   * Parse all mappings. Mappings should be located besides the jar file in a
   * subfolder called 'mapping'. Subfolders will not be supported. Mappings
   * should be placed in a file with suffix '_mapping.json'
   */
  private void parseAllMappings() {
    Path pathToMappings = Paths.get(mappingsDir).toAbsolutePath();
    LOGGER.debug("Parse all files with suffix '{}' in folder '{}'", mappingsSuffix, pathToMappings);
    allMappings = new HashMap<>();
    try {
      Stream<Path> list = Files.list(pathToMappings);
      Gson gson = new GsonBuilder()
              .setPrettyPrinting()
              .disableHtmlEscaping()
              .create();
      list.filter(file -> file.toString().endsWith(mappingsSuffix)).forEach(path -> {
        LOGGER.debug("Read mapping from file: '{}'", path.getFileName());
        try {
          JsonReader reader;
          reader = new JsonReader(new FileReader(path.toFile()));
          HttpMapping mappingSchema = gson.fromJson(reader, HttpMapping.class);
          LOGGER.debug("Mapping: '{}'", gson.toJson(mappingSchema));
          if (mappingSchema.getClientId() != null) {
            // add mapping to map
            allMappings.put(mappingSchema.getClientId(), mappingSchema);
//            allMappings.put(mappingSchema.getBaseUrl(), mappingSchema);
            allMappings.put(DEFAULT_MAPPING, mappingSchema);
          }
        } catch (IOException ex) {
          LOGGER.error("Error reading mapping from file '{}'!", path.getFileName());
        }
      });
    } catch (IOException ex) {
      LOGGER.error("Error reading mapping dir '{}'!", pathToMappings);
    }
  }

  /**
   * Get mapping for given client ID.
   * @param clientId Client ID of mapping.
   * @return Mapping for given client ID.
   */
  public static HttpMapping getMapping(String clientId) {
    return allMappings.getOrDefault(clientId, allMappings.get(DEFAULT_MAPPING) );
  }

  /**
   * Get list of all client IDs.
   * @return List of all client IDs. 
   */
  public static List<String> getMappings() {
    List<String> mappings = new ArrayList<>(allMappings.keySet());
    mappings.remove(DEFAULT_MAPPING);
    return mappings;
  }
}
