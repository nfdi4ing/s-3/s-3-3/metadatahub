package edu.kit.metadatahub.rest.controller;

import edu.kit.metadatahub.doip.server.MetadataHub;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Main class for Spring Boot application.
 */
@SpringBootApplication
@ComponentScan({"edu.kit.metadatahub.rest.controller"})
public class Rest4MetadataHub {
  /** 
   * Main method of Spring Boot application.
   * @param args commandline arguments.
   * @throws Exception any exception.
   */
  public static void main(String[] args) throws Exception {
    SpringApplication.run(Rest4MetadataHub.class, args);
    System.out.println("Start DOIP server......");
    MetadataHub.main(args);
  }
}
