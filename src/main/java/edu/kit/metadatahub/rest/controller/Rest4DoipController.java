package edu.kit.metadatahub.rest.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.kit.metadatahub.doip.mapping.Mapping2HttpService;
import edu.kit.metadatahub.doip.rest.Content;
import edu.kit.metadatahub.doip.rest.Operations;
import edu.kit.metadatahub.doip.rest.RestDoip;
import edu.kit.metadatahub.doip.server.util.DoipUtil;
import edu.kit.metadatahub.doip.server.util.MappingUtil;
import edu.kit.rest.util.SimpleServiceClient;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import java.io.IOException;
import net.dona.doip.client.DoipException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * REST controller for accessing all available mappings.
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@ApiResponses(value = {
  @ApiResponse(responseCode = "200", description = "The request was processed successfully."),
  @ApiResponse(responseCode = "400", description = "There was something wrong with the structure or content of the request"),
  @ApiResponse(responseCode = "401", description = "The client must authenticate to perform the attempted operation"),
  @ApiResponse(responseCode = "403", description = "The client was not permitted to perform the attempted operation"),
  @ApiResponse(responseCode = "404", description = "The requested digital object could not be found"),
  @ApiResponse(responseCode = "500", description = "There was an internal server error")})
public class Rest4DoipController {

  /**
   * Logger.
   */
  private final static Logger LOGGER = LoggerFactory.getLogger(SimpleServiceClient.class);
  
  private static final String HIGHLIGHT_TRACE_MESSAGE = "***************************************************************************";

  /**
   * Get all mapping IDs defined in mapping configuration directory.
   *
   * @param operationId
   * @param digitalObject
   * @return Array containing all mappingIDs.
   * @throws net.dona.doip.client.DoipException
   * @throws java.io.IOException
   */
  @Operation(summary = "REST interface based on DOIP.", description = "REST interface based on DOIP to enable easier access for websites.")
  @RequestMapping(value = {"/doip"}, method = {RequestMethod.POST}, consumes = {MediaType.APPLICATION_JSON_VALUE})
  @ResponseBody
  public ResponseEntity postDoipOperation(@Parameter(description = "The operationId.", required = true) @RequestParam(value = "operationId") Operations operationId,
          @Parameter(description = "Json representation of the Digital Object.", required = false) @RequestBody(required = false) final RestDoip digitalObject) throws DoipException, IOException {
    Object returnValue;
    returnValue = "Erro: No valid operation! (" + operationId + ")";
    DoipUtil doipUtil = new DoipUtil(digitalObject);

    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    String jsonStr = gson.toJson(digitalObject);

    // Displaying JSON String on console
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace(HIGHLIGHT_TRACE_MESSAGE);
      LOGGER.trace("****  REST4DOIP");
      LOGGER.trace(HIGHLIGHT_TRACE_MESSAGE);
      LOGGER.trace("Operation ID: " + operationId);
      LOGGER.trace("Target ID   : " + digitalObject.getTargetId());
      LOGGER.trace("JSON4DOIP   :");
      LOGGER.trace(jsonStr);
      for (int index = 0; index < 3; index++) {
        LOGGER.trace(HIGHLIGHT_TRACE_MESSAGE);
      }
    }
    Mapping2HttpService service = new Mapping2HttpService();
    service.initMapping(MappingUtil.getMapping(digitalObject.getClientId()));
    switch (operationId) {
      case OP_CREATE -> returnValue = service.createViaRest(doipUtil);
      case OP_RETRIEVE -> returnValue = service.retrieveViaRest(doipUtil);
      case OP_UPDATE -> returnValue = service.updateViaRest(doipUtil);
      case OP_VALIDATE -> returnValue = service.validateViaRest(doipUtil);
      case OP_DELETE -> {
        // not supported yet
      }
      case OP_SEARCH -> returnValue = service.searchViaRest(doipUtil);
      default -> {
        // Ouch, not possible
      }
    }

    jsonStr = gson.toJson(returnValue);
    ResponseEntity.BodyBuilder status = ResponseEntity.status(HttpStatus.OK);
    if (returnValue instanceof RestDoip restDoip) {
      for (Content content : restDoip.getHeader()) {
        status.header(content.getId(), content.getValue());
      }
    }
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace(HIGHLIGHT_TRACE_MESSAGE);
      LOGGER.trace("**** Return");
      LOGGER.trace(HIGHLIGHT_TRACE_MESSAGE);
      LOGGER.trace(jsonStr);
      for (int index = 0; index < 3; index++) {
        LOGGER.trace(HIGHLIGHT_TRACE_MESSAGE);
      }
    }

    return status.body(jsonStr);
  }
}
