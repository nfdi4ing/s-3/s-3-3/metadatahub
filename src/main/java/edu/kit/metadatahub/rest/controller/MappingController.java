package edu.kit.metadatahub.rest.controller;

import edu.kit.metadatahub.doip.server.util.MappingUtil;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
/**
 * REST controller for accessing all available mappings.
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class MappingController {
  /**
   * Array holding all available mappings.
   */
  private static final List<String> mappings = new ArrayList<>();
  
  /**
   * Get all mapping IDs defined in mapping configuration directory.
   * @return Array containing all mappingIDs.
   */
  @Operation(summary = "Get all mappings of MetadataHub.", description = "List all available mappings.",
          responses = {
            @ApiResponse(responseCode = "200", description = "OK and a list of mappings or an empty list if no mappings available.", content = @Content(array = @ArraySchema(schema = @Schema(implementation = String.class))))})
  @RequestMapping(value = {"/api/v1/mapping"}, method = {RequestMethod.GET})
  @ResponseBody
	public List<String> getMappings() {
		return MappingUtil.getMappings();
	}
}