/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package edu;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import static edu.kit.metadatahub.doip.mapping.IMappingInterface.LOGGER;
import static edu.kit.metadatahub.doip.mapping.Mapping2HttpService.replacePlaceholders;
import edu.kit.metadatahub.doip.mapping.metadata.IMetadataMapper;
import edu.kit.metadatahub.doip.server.util.DoipUtil;
import edu.kit.rest.util.SimpleServiceClient;
import edu.kit.turntable.mapping.Datacite43Schema;
import edu.kit.turntable.mapping.Pid;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import static java.util.stream.Collectors.mapping;
import net.dona.doip.DoipConstants;
import net.dona.doip.client.DigitalObject;
import net.dona.doip.client.DoipException;
import net.dona.doip.client.Element;
import net.dona.doip.util.GsonUtility;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

/**
 *
 * @author hartmann-v
 */
public class Test {

  private static final String COSCINE_METADATA_V1 = "{\n"
          + "\"http://purl.org/dc/terms/creator\": [{\n"
          + "\"value\": \"Volker Hartmann\",\n"
          + "\"type\": \"literal\",\n"
          + "      \"datatype\": \"http://www.w3.org/2001/XMLSchema#string\"\n"
          + "    }],\n"
          + "  \"http://purl.org/dc/terms/title\": [{\n"
          + "      \"value\": \"Test\",\n"
          + "      \"type\": \"literal\",\n"
          + "      \"datatype\": \"http://www.w3.org/2001/XMLSchema#string\"\n"
          + "    }],\n"
          + "  \"http://purl.org/dc/terms/created\": [{\n"
          + "      \"value\": \"2022-05-02\",\n"
          + "      \"type\": \"literal\",\n"
          + "      \"datatype\": \"http://www.w3.org/2001/XMLSchema#date\"\n"
          + "    }]\n"
          + "}\n";

  public static void main(String[] args) {
    String bearerToken = "noToken";
      // Read bearer token from file...
      InputStream inputStream = null;
      try {
        File file = new File("/home/hartmann-v/Schreibtisch/token.txt");
        inputStream = new FileInputStream(file);

        try ( BufferedReader br
                = new BufferedReader(new InputStreamReader(inputStream))) {
          String line;
          line = br.readLine();
          bearerToken = /*"bearer " + */ line;
        }
      } catch (IOException ioe) {
        System.out.println("Error reading " + args[0]);
        bearerToken = "anInvalidToken";
      } finally {
        if (inputStream != null) {
          try {
            inputStream.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
    HttpStatus resource = null;
    LOGGER.debug("Repo: prepare REST call ...");
    // First of all get targetId.
//    metadata.setPid(pid);

    String baseUrl = "https://coscine.rwth-aachen.de/coscine/api/Coscine.Api.Tree/Tree/62b97a86-d3cf-4517-9b09-6a09cd9b476d?path=/newfile.txt";
    if (baseUrl != null) {
      LOGGER.trace("baseURL: '{}'", baseUrl);
      String acceptType = "text/turtle";

      SimpleServiceClient simpleClient = SimpleServiceClient.create(baseUrl);
      simpleClient.accept(MediaType.parseMediaType(acceptType));
      System.out.println(bearerToken);
      // Add authentication if available
      simpleClient.withBearerToken(bearerToken);
      ///////////////////////////////////////////////////////////////
      // Prepare metadata
      ///////////////////////////////////////////////////////////////
      ///////////////////////////////////////////////////////////////
      // Prepare headers
      // header with values will be assigned to header send.
      // header without values will be assigned to collect headers.
      ///////////////////////////////////////////////////////////////
      ///////////////////////////////////////////////////////////////
      // Prepare request
      ///////////////////////////////////////////////////////////////
      // add entries to form
//          for (String key : streamMap.keySet()) {
//            LOGGER.trace("Found stream: '{}' -> '{}'", key, streamMap.get(key).length);
//            String mappingKey = key;
//            if (mapping.getBody().getAdditionalProperties().containsKey(key)) {
//              mappingKey = mapping.getBody().getAdditionalProperties().get(key);
//            }
//            InputStream documentStream = new ByteArrayInputStream(streamMap.get(key));
//            simpleClient.withFormParam(mappingKey, documentStream);
//          }
//          String mappingKey = DoipUtil.ID_METADATA;
//          if (mapping.getBody() != null) {
//            if (mapping.getBody().getMetadata() != null) {
//              mappingKey = mapping.getBody().getMetadata();
//            }
//            simpleClient.withFormParam(mappingKey, metadata);
//          }
      ///////////////////////////////////////////////////////////////
      // Make request
      ///////////////////////////////////////////////////////////////
      String resourceString = simpleClient.putResource(COSCINE_METADATA_V1, String.class, false); //Resource(srs, SchemaRecordSchema.class);
      System.out.println("#############################################################");
      System.out.println(resourceString);
      System.out.println(simpleClient.getResponseStatus());
      System.out.println("#############################################################");
      ///////////////////////////////////////////////////////////////
      // Collect response
      ///////////////////////////////////////////////////////////////
    }
  }

}
