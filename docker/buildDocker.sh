#!/bin/bash
################################################################################
# Build docker locally
# Usage:
# bash buildDocker.sh hostname [TAG_NAME] 
################################################################################

################################################################################
# Define default values for variables
################################################################################
TAG_NAME=local

################################################################################
# START DECLARATION FUNCTIONS
################################################################################

################################################################################
function usage {
################################################################################
  echo "USAGE:"
  echo " $0 hostname [TAG_NAME]"
  exit 1
}

################################################################################
function printInfo {
################################################################################
  echo "---------------------------------------------------------------------------"
  echo "$*"
  echo "---------------------------------------------------------------------------"
}

################################################################################
# END DECLARATION FUNCTIONS / START OF SCRIPT
################################################################################

################################################################################
# Test for commands used in this script
################################################################################
testForCommands="dirname date docker git"

for command in $testForCommands
do 
  if ! type $command >> /dev/null; then
    echo "Error: command '$command' is not installed!"
    exit 1
  fi
done

################################################################################
# Determine directory of script. 
################################################################################
ACTUAL_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

################################################################################
# Determine repo name 
################################################################################
REPO_NAME=metadatahub

################################################################################
# Determine tag 
# 1. use tagname given as argument
# 2. determine last tag of git
#    a) if no tag defined -> local
#    b) concat actual date to tag
################################################################################
# Check no of parameters.
if [ $# -eq 0 ]; then
  echo You have provided zero arguments. At least hostname is required.
  usage
  exit 1
fi

if [ -n "$1" ]; then
  HOSTNAME=$1
else
  echo Missing argument. At least hostname is required.
  usage
  exit 1
fi

if [ $# -gt 2 ]; then 
  echo "Illegal number of parameters!"
  usage
  exit 1
fi
if [ -n "$2" ]; then
  TAG_NAME=$2
  # Check for invalid flags
  if [ "${TAG_NAME:0:1}" = "-" ]; then
    usage
  fi
else
  LAST_TAG=$(git describe --abbrev=0 --tags) >> /dev/null
  if [ "$LAST_TAG" = "" ]; then
    LAST_TAG=$TAG_NAME
  fi
  TAG_NAME=$LAST_TAG-`date -u +%Y-%m-%d`
fi

cd $ACTUAL_DIR/.. || { echo "Failure changing to directory $ACTUAL_DIR/.."; exit 1; }

################################################################################
# Build local docker
################################################################################
printInfo Build docker container kitdm/$REPO_NAME:$TAG_NAME 

if ! docker build --build-arg HOSTNAME_DEFAULT=$HOSTNAME -t kitdm/$REPO_NAME:$TAG_NAME .; then
  echo .
  printInfo "ERROR while building docker container!"
  usage
else 
  echo .
  printInfo Now you can create and start the container by calling docker "run -d -p8888:8888 -p8890:8890 --name metadataHub kitdm/$REPO_NAME:$TAG_NAME"
fi