# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Mapping for AIMS
- Acknowledgements
- CITATION.cff

### Changed
- Mapping for Coscine

### Fixed

## [v0.9.1]
First release.

### Added
- Configuration via json file.
- Dockerfile for dockerization.

### Changed

### Fixed


[Unreleased]: https://git.rwth-aachen.de/nfdi4ing/s-3/s-3-3/metadatahub/compare/v0.9.1...HEAD
[v0.9.1]: https://git.rwth-aachen.de/nfdi4ing/s-3/s-3-3/metadatahub/compare/v0.9.0...v0.9.1
